﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSME : MonoBehaviour {
	//position
	public Transform tf;
	public Transform otherObject;
	public Transform tr;

	public Vector3 direction;
	public float speed;

	public Transform player;

	//States
	public enum AIStates {Start, TurnRed, TurnGreen };
	public AIStates current;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		player = GameObject.FindWithTag ("Player").transform;
		tr = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		switch (current) {
		case AIStates.Start:

			DoStart ();

			if (Vector3.Distance (tf.position, otherObject.position) < 5) {
				current = AIStates.TurnRed;
			}
			if (Vector3.Distance (tf.position, otherObject.position) > 10) {
				current = AIStates.TurnGreen;
			}
			break;
		case AIStates.TurnRed:
			DoTurnRed ();
			if (Vector3.Distance (tf.position, otherObject.position) > 10) {
				current = AIStates.TurnGreen;
			}
			break;
		case AIStates.TurnGreen:
			DoTurnGreen ();
			if (Vector3.Distance (tf.position, otherObject.position) < 5) {
				current = AIStates.TurnRed;
			}
			break;
		}
	}

	void DoStart (){
	//do nothing
	}

	void DoTurnRed (){
		SpriteRenderer sr;
		sr = GetComponent<SpriteRenderer> ();
		sr.color = Color.red;

		//Find Vecter betewn us and the target, find -1 of that vecter, set our up to the mew vecter
		direction = (player.position - transform.position).normalized;
		tr.position = tr.position + (direction * speed);
	}

	void DoTurnGreen (){
		SpriteRenderer sr;
		sr = GetComponent<SpriteRenderer> ();
		sr.color = Color.green;
	}
}
