﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManger : MonoBehaviour {

	public static UIManger uiManger;
	public GameObject mainMuen;
	public GameObject loseMuen;

	void Awake() {
		if (uiManger == null) {
			uiManger = this;
		} else {
			Destroy (this);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void MainMuen () {
		mainMuen.SetActive (true);
		loseMuen.SetActive (false);
	}

	public void LoseMuen () {
		mainMuen.SetActive (false);
		loseMuen.SetActive (true);
	}

	public void StartGame () {
		mainMuen.SetActive (false);
		loseMuen.SetActive (false);
	}
}
