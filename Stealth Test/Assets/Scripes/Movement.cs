﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public Transform tf;
	public float speed;
	public float turnSpeed;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W)) {
			//tf.position += tf.TransformDirection(new Vector3(1,0,0)) * speed;
			tf.position += tf.up * speed;
		}
		if (Input.GetKey (KeyCode.S)) {
			//tf.position += tf.TransformDirection(new Vector3(1,0,0)) * speed;
			tf.position += -tf.up * speed;
		}
		if (Input.GetKey (KeyCode.D)) {
			tf.Rotate (0,0,-turnSpeed);
		}
		if (Input.GetKey (KeyCode.A)) {
			tf.Rotate (0,0,turnSpeed);
		}
	}
}