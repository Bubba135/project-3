﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIseces : MonoBehaviour {

	public float hearingModifier; //0 is death, 1 is normol, 2 twice the power. 


	public float fieldOfview;
	public float lineOfSight;
	public float viewDistance;

	//call this function when Ai actively
	public bool canHear (GameObject target){
		//is that object capable making noise?
		hearing noiseMaker = target.GetComponent<hearing>();
		if (noiseMaker == null) {
			return false;
		}
		//is it making noise
		if (noiseMaker.volume <= 0) {
			return false;
		}
		//is the noise loud enough
		float distance = Vector3.Distance(GetComponent<Transform>().position, target.GetComponent<Transform> ().position);

		//calculate the modified required distance
		float modDistance = noiseMaker.volume * hearingModifier;

		//check if close
		if (distance <= modDistance) {
			return true;
		}

		//otherwise
		return false;
	}


	public bool CanSee ( GameObject target ) {
		//field of view
		Vector3 forwardVector = GetComponent<Transform>().forward;
		Vector3 vectorToTarget = target.GetComponent<Transform> ().position - GetComponent<Transform> ().position;

		float angleToTarget = Vector3.Angle (forwardVector, vectorToTarget);
		if (angleToTarget > fieldOfview) {
			return false;
		}

		//line of sight
		RaycastHit2D hitInfo = Physics2D.Raycast(GetComponent<Transform>().position, vectorToTarget, viewDistance);
		//no hit no seeing
		if (hitInfo == null) {
			return false;
		}

		//if I can see the object
		if (hitInfo.collider.gameObject == target) {
			return true;
		}

		//otherwise
		return false;
	}

}
