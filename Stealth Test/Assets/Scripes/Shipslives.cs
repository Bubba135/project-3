﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shipslives : MonoBehaviour {

	public float deathforce;
	public int lives;
	public Color inColor;
	public Color normalColor;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void Respawn()
	{
		rb.velocity = Vector2.zero;
		transform.position = Vector2.zero;

		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		sr.enabled = true;
		sr.color = inColor;
		Invoke("InvuInerble", 3f);

	}

	void InvuInerble()
	{
		GetComponent<Collider2D>().enabled = true;
		GetComponent<SpriteRenderer>().color = normalColor;
	}

	void lossLife()
	{
		lives--;
		//Repawn
		GetComponent<SpriteRenderer>().enabled = false;
		GetComponent<Collider2D>().enabled = false;
		Invoke ("Respawn", 3f);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Debug.Log (col.relativeVelocity.magnitude);
		if (col.relativeVelocity.magnitude > deathforce) {
			lossLife ();


			if(lives <= 0)
			{
				//game over
				GameOver();
			}
		}
	}

	void GameOver()
	{
		CancelInvoke ();
	}
}
